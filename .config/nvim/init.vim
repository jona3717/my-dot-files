filetype plugin on
set nocp
packadd! dracula
syntax on
colorscheme dracula "oceanic_material
let g:airline_theme='deus'
let python_highlight_all=1
let mapleader=" "
let NERDTreeQuitOnOpen=1
set ai "Permite la indentación"
set tabstop=4 "Estaplece un tabulador en 4 espacios"
set shiftwidth=4 "Establece en 4 espacios la autoindentación"
set softtabstop=4
"set textwidth=79
set expandtab
set autoindent
set fileformat=unix
set nu "Muestra números de línea"
set cindent "Indenta con la forma de C"
set mouse=a "Activa uso del mouse"
set splitbelow
set encoding=utf-8
set modifiable
set showcmd
set clipboard=unnamed
set ruler
set showmatch
set relativenumber
"autocmd vimenter * NERDTree \"Abre NerdTree automáticamente"
"Marca los espacios en blanco innecesarios, como errores"
"au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/ 

"Vim transition
set runtimepath^=~/.vim runtimepath+=~/.vim/after
    let &packpath = &runtimepath
    source ~/.vimrc

"split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

"configure navigation keys to dvorak
set langmap=ej,je,tl,lt,ku,uk

" Enable folding
set foldmethod=syntax
set foldlevel=99
"Funciona con za

"Atajos"
nmap <Leader>nl :NERDTree<CR>
nmap <Leader>w :w<CR>
nmap <Leader>q :q<CR>
vmap // :s:^://<CR>

"####################### Start Lightline settings ##################"
let g:lightline= {
    \ 'active': {
    \ 'left': [['mode', 'paste'], ['relativepath', 'modified']],
    \ 'right': [['kitestatus'], ['filetype', 'percent', 'lineinfo'], ['gitbranch']]
    \ },
    \ 'inactive': {
    \ 'left': [['inactive'], ['relativepath']],
    \ 'right': [['bufnum']]
    \ },
    \ 'component_function': {
    \   'gitbranch': 'FugitiveHead',
    \   'kitestatus': 'kite#statusline'
    \ },
    \ 'colorscheme': 'jellybeans',
    \ 'subseparator': {
    \ 'left': '',
    \ 'right': ''
    \ },
    \ }
"####################### end Lightline settings ##################"

"################################# c-support######################"

set tags+=~/.vim/tags/stl
noremap :!ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .<cr>
inoremap :!ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .<cr>
let OmniCpp_NamespaceSearch = 1
let OmniCpp_GlobalScopeSearch = 1
let OmniCpp_ShowAccess = 1
let OmniCpp_MayCompleteDot = 1
let OmniCpp_MayCompleteArrow = 1
let OmniCpp_MayCompleteScope = 1
let OmniCpp_DefaultNamespaces = ["std", "_GLIBCXX_STD"]
au CursorMovedI,InsertLeave * if pumvisible() == 0|silent! pclose|endif
set completeopt=menuone,menu,longest,preview

"######################## end c-support ###########################"

"####################### Start kite settings ##################"
nnoremap <c-space> <C-x><C-u>
" All the languages Kite supports
let g:kite_supported_languages = ['*']
"Set tab to insert the currently selected completion
let g:kite_tab_complete=1
"Set navigation between placeholders
let g:kite_previous_placeholder = '<C-E>'
let g:kite_next_placeholder = '<C-U>'
"Show on status line what Kite is doing
set statusline=%<%f\ %h%m%r%{kite#statusline()}%=%-14.(%l,%c%V%)\ %P
set laststatus=2  " always display the status line
"####################### end Kite settings ##################"


"####################### Plug gestor de plugins ##################"

call plug#begin('~/.config/nvim/plugged')
Plug 'dracula/vim'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'preservim/nerdtree'
Plug 'vim-syntastic/syntastic'
Plug 'nvie/vim-flake8'
Plug 'tpope/vim-fugitive'
Plug 'glepnir/oceanic-material'
Plug 'itchyny/lightline.vim'
Plug 'jiangmiao/auto-pairs'
call plug#end()
"####################### end Plug ##################"
