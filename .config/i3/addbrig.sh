#!/bin/bash

brillo=$(cat /sys/class/backlight/intel_backlight/brightness)
nbrillo=$(expr $brillo + 85)
echo $nbrillo > /sys/class/backlight/intel_backlight/brightness
